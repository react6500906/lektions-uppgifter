import React from 'react';

function MemberCard({ name, city, image }) {
  return (
    <div>
      <img src={image} alt={name} style={{ width: 100, height: 100 }} />
      <h2>{name}</h2>
      <p>{city}</p>
    </div>
  );
}

export default MemberCard;
