import { useState } from 'react';
import image from "./../assets/image.jpg";
import Hero from './Hero'
import Main from './Main'

const data = [
  {
    id: 1,
    name: "Ysme",
    city: "Bangladesh",
    image: image,
  },
  {
    id: 2,
    name: "Rana",
    city: "Osla",
    image: image,
  },
  {
    id: 3,
    name: "Ynve",
    city: "Omaha",
    image: image,
  },
];


function Appen() {
  const [members, setMembers] = useState(data)
  const [groupName, setGroupName] = useState('Min Studiegrupp');
  const [color, setColor] = useState('black');

  const toggleColor = () => {
    setColor(prevColor => prevColor === 'black' ? 'blue' : 'black');
  };

  

  return (
    <div>
      <Hero groupName={groupName} toggleColor={toggleColor} color={color} />
      <Main members={members} />
    </div>
  );
}

export default Appen
