import React from "react";
import MemberCard from "./MemeberCard";



function Main({ members }) {
  return (
    <main>
      <p>Välkommen till vårt projekt! Här kan ni hitta information om vad vi arbetar med.</p>
      {members.map(member => (
        <MemberCard key={member.id} name={member.name} city={member.city} image={member.image}></MemberCard>
      ))}
    </main>
  );
}

export default Main;
