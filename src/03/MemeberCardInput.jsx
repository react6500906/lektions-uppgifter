import React, { useState } from "react";

function MemberCardInput({ member, setMembers }) {
  const [formData, setFormData] = useState({
    name: "",
    city: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (e, id) => {
    e.preventDefault();
    setMembers((prev) =>
      prev.map((member) => {
        if (member.id === id) {
          return { ...member, ...formData };
        }
        return member;
      })
    );
  };

  return (
    <>
      <img
        src={member.image}
        alt={member.name}
        style={{ width: 100, height: 100 }}
      />
      <h2>{member.name}</h2>
      <p>{member.city}</p>
      <form onSubmit={(e) => handleSubmit(e, member.id)}>
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={handleChange}
        />
        <input
          type="text"
          name="city"
          value={formData.city}
          onChange={handleChange}
        />
        <button type="submit">Spara</button>
      </form>
    </>
  );
}

export default MemberCardInput;
