import React, { useState } from 'react';
import MemberCardInput from './MemeberCardInput';
import MemberCardClick from './MemeberCardClick';


function Main({ members, setMembers }) {
  const [searchTerm, setSearchTerm] = useState('');

  const filteredMembers = members.filter(
    member => member.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
              member.city.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <main>
      <input
        type="text"
        placeholder="Sök medlemmar efter namn eller stad..."
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      {filteredMembers.map(member => (
         <MemberCardClick key={member.id} member={member} setMembers={setMembers}></MemberCardClick>
      ))}
    </main>
  );
}

export default Main;
