import React, { useState } from "react";

function MemberCardClick({ member, setMembers }) {
    const [updatedInfo, setUpdatdeInfo] = useState({name: "Hej", city: "Då"})

    function handleUpdate(id){
        setMembers((prev) =>
        prev.map((member) => {
          if (member.id === id) {
            return { ...member, ...updatedInfo };
          }
          return member;
        })
      );
    }

  return (
    <div onClick={() => handleUpdate(member.id)}>
      <img src={member.image} alt={member.name} style={{ width: 100, height: 100 }} />
      <h2>{member.name}</h2>
      <p>{member.city}</p>
    </div>
  );
}

export default MemberCardClick;
