import React from "react";

function Main() {
  return (
    <main>
      <p>
        Välkommen till vårt projekt! Här kan ni hitta information om vad vi
        arbetar med.
      </p>
    </main>
  );
}

export default Main;
