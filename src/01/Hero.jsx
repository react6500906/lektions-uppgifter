import React from 'react'
const navigationLinks = ['Hem', 'Om oss', 'Projekt', 'Kontakt'];

function Hero() {
    const [groupName, setGroupName] = useState('Min Studiegrupp');
    const [color, setColor] = useState('black');
  
    const toggleColor = () => {
      setColor(prevColor => prevColor === 'black' ? 'blue' : 'black');
    };
  return (
    <div>
    <nav>
      <ul>
        {navigationLinks.map(link => <li key={link}><a href="#">{link}</a></li>)}
      </ul>
      <h1 onClick={toggleColor} style={{ color }}>{groupName}</h1>
    </nav>
  </div>
  )
}

export default Hero