
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LandingPage from "./landing-page/LandingPage";
import NotFound from "./NotFound";


function Appen() {


  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage/>} />
        <Route path="/*" element={<NotFound/>} />
      </Routes>
    </BrowserRouter>
  );
}

export default Appen;
