import React, { useEffect, useState } from 'react';
import MemberCardInput from './MemeberCardInput';
import AddNewMemeber from './AddNewMemeber';


function Main({ members, setMembers }) {
  const [searchTerm, setSearchTerm] = useState('');

  const filteredMembers = members.filter(
    member => member.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
              member.city.toLowerCase().includes(searchTerm.toLowerCase())
  );



  const handleAddMember = (newMember) => {
    setMembers(prevState => [...prevState, newMember]);
  };

  return (
    <main>
    <div>
      <AddNewMemeber handleAddMember={handleAddMember} setSearchTerm={setSearchTerm} searchTerm={searchTerm}> </AddNewMemeber>
      </div>
      {filteredMembers.map(member => (
         <MemberCardInput key={member.id} member={member} setMembers={setMembers}></MemberCardInput>
      ))}
    </main>
  );
}

export default Main;
