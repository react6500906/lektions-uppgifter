import axios from "axios";
import React, { useState } from "react";

function AddNewMemeber({handleAddMember, setSearchTerm, searchTerm}) {
    const [newMember, setNewMember] = useState({ name: '', city: '' });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setNewMember(prevState => ({ ...prevState, [name]: value }));
      };

      async function onSubmit(e) {
        e.preventDefault();
        try {
          await axios.post('http://localhost:3000/members', newMember);
          handleAddMember(newMember);
          setNewMember({ name: '', city: '' });
        } catch (error) {
          console.error(error);
        }
      }
    
  return (
    <>
      <input
        type="text"
        placeholder="Sök medlemmar efter namn eller stad..."
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <div>
        <input
          type="text"
          placeholder="Namn"
          name="name"
          value={newMember.name}
          onChange={handleInputChange}
        />
        <input
          type="text"
          placeholder="Stad"
          name="city"
          value={newMember.city}
          onChange={handleInputChange}
        />
        <button onClick={onSubmit}>Lägg till medlem</button>
      </div>
    </>
  );
}

export default AddNewMemeber;
