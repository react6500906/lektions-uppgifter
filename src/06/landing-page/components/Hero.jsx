import React from 'react';

const Hero = ({ groupName, toggleColor, color }) => {
  const navigationLinks = ['Hem', 'Om oss', 'Projekt', 'Kontakt'];

  return (
    <div>
      <nav>
        <ul>
          {navigationLinks.map(link => <li key={link}><a href="#">{link}</a></li>)}
        </ul>
        <h1 onClick={toggleColor} style={{ color }}>{groupName}</h1>
      </nav>
    </div>
  );
}

export default Hero;
