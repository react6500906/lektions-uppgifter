import React, { useEffect, useState } from 'react'
import Hero from './components/Hero';
import Main from './components/Main';
import axios from 'axios';




function LandingPage() {
    const [members, setMembers] = useState([]);
    const [groupName, setGroupName] = useState("Min Studiegrupp");
    const [color, setColor] = useState("black");
  


      const fetchData = async () => {
        try {
          const response = await axios.get('http://localhost:3000/members');
          setMembers(response.data);
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };
  
      useEffect(() => {
        fetchData();
      }, []);
  

    const toggleColor = () => {
      setColor((prevColor) => (prevColor === "black" ? "blue" : "black"));
    };
  return (
    <div>
        <Hero groupName={groupName} toggleColor={toggleColor} color={color} />
        <Main members={members} setMembers={setMembers} />
    </div>
  )
}

export default LandingPage