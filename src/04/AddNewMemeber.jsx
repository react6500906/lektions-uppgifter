import React, {useState} from "react";

function AddNewMemeber({handleAddMember}) {
    const [newMember, setNewMember] = useState({ name: '', city: '' });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setNewMember(prevState => ({ ...prevState, [name]: value }));
      };

      function onSubmit(e) {
        e.preventDefault();
        handleAddMember(newMember);
        setNewMember({ name: '', city: '' });
      }
  return (
    <>
      <div>
        <input
          type="text"
          placeholder="Namn"
          name="name"
          value={newMember.name}
          onChange={handleInputChange}
        />
        <input
          type="text"
          placeholder="Stad"
          name="city"
          value={newMember.city}
          onChange={handleInputChange}
        />
        <button onClick={onSubmit}>Lägg till medlem</button>
      </div>
    </>
  );
}

export default AddNewMemeber;
